package test.oui.xspeedit.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.oui.xspeedit.service.impl.BuildBoxesServiceImpl;

@RestController
@RequestMapping("/api")
public class XspeedItController {
	
	@Autowired
	BuildBoxesServiceImpl buildBoxesServiceImpl;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/boxes/{chain}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getOptimisedBoxes(@PathVariable String chain) {

		try {
			List<String> result = buildBoxesServiceImpl.buildBoxes(chain);
			return new ResponseEntity<List<String>>(result, HttpStatus.OK);
			
		} catch (IllegalArgumentException e) {
			return new ResponseEntity<List<String>>(HttpStatus.BAD_REQUEST);
		}
	}
}
